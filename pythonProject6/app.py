from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from config import DB_USERNAME, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = (
    f"mysql+mysqlconnector://{DB_USERNAME}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
)
db = SQLAlchemy(app)

class Guess(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    animal_name = db.Column(db.String(100), nullable=False)

ANIMALS = [
    {"name": "Dog", "image_url": "dog.jpeg", "guessed": False},
    {"name": "Cat", "image_url": "cat.jpeg", "guessed": False},
    {"name": "Elephant", "image_url": "elephant.jpg", "guessed": False},
    {"name": "Bird", "image_url": "bird.jpg", "guessed": False}
]

@app.route('/')
def index():
    return render_template('index.html', animals=ANIMALS)

@app.route('/animal/<string:animal_name>', methods=['POST'])
def animal(animal_name):
    guessed_name = request.form['guessed_name']
    for animal in ANIMALS:
        if animal['name'] == animal_name:
            if guessed_name.lower() == animal['name'].lower():
                animal['guessed'] = True
                # Store the wrong guess in the database
                wrong_guess = Guess(animal_name=guessed_name)
                db.session.add(wrong_guess)
                db.session.commit()
                return redirect(url_for('animal_info', animal_name=animal_name))
            else:
                return redirect(url_for('index'))

@app.route('/animal/<string:animal_name>/info')
def animal_info(animal_name):
    animal = next((a for a in ANIMALS if a['name'] == animal_name), None)
    if animal and animal.get('guessed', False):
        return render_template('info.html', animal=animal)
    else:
        return redirect(url_for('index'))

@app.route('/reset')
def reset_guesses():
    for animal in ANIMALS:
        animal['guessed'] = False
    return redirect(url_for('index'))

if __name__ == '__main__':
    db.create_all()  # Create the database tables
    app.run(host='0.0.0.0', port=5000)
